package service

import (
	"log"
	"time"

	"github.com/muhammadyusuf22/testing-backend-majoo/dto"
	"github.com/muhammadyusuf22/testing-backend-majoo/entity"
	"github.com/muhammadyusuf22/testing-backend-majoo/repository"

	"github.com/mashingan/smapping"
	"golang.org/x/crypto/bcrypt"
)

type AuthService interface {
	VerifyCredential(username string, password string) interface{}
	CreateUser(user dto.RegisterDTO) entity.User
	FindByUserName(username string) entity.User
	IsDuplicateUserName(username string) bool
}

type authService struct {
	userRepository repository.UserRepository
}

func NewAuthService(userRep repository.UserRepository) AuthService {
	return &authService{
		userRepository: userRep,
	}
}
func (service *authService) VerifyCredential(username string, password string) interface{} {
	res := service.userRepository.VerifyCredential(username, password)
	if v, ok := res.(entity.User); ok {
		comparedPassword := comparedPassword(v.Password, []byte(password))
		if v.UserName == username && comparedPassword {
			return res
		}
		return false
	}
	return false
}

func (service *authService) CreateUser(user dto.RegisterDTO) entity.User {
	userToCreate := entity.User{}
	err := smapping.FillStruct(&userToCreate, smapping.MapFields(&user))
	if err != nil {
		log.Fatalf("Failed map %v", err)
	}
	userToCreate.CreatedBy = 0
	userToCreate.CreatedAt = time.Now()
	res := service.userRepository.InsertUser(userToCreate)
	return res
}

func (service *authService) FindByUserName(username string) entity.User {
	return service.userRepository.FindByUsername(username)
}

func (service *authService) IsDuplicateUserName(username string) bool {
	res := service.userRepository.IsDuplicateUserName(username)
	return !(res.Error == nil)
}

func comparedPassword(hashedPwd string, plainPassword []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPassword)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
