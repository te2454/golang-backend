package repository

import (
	"database/sql"
	"fmt"
	"github.com/muhammadyusuf22/testing-backend-majoo/dto"
	"github.com/muhammadyusuf22/testing-backend-majoo/models"
	"gorm.io/gorm"
)

type LaporanRepository interface {
	FindListLaporanMerchantOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantDto, int64, error)
	FindListLaporanMerchantOutletOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantOutletDto, int64, error)
}

type laporanConnection struct {
	connection *gorm.DB
}

func NewLaporanRepository(db *gorm.DB) LaporanRepository {
	return &laporanConnection{
		connection: db,
	}
}

func (db *laporanConnection) FindListLaporanMerchantOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantDto, int64, error) {
	var omzetMerchantDto []dto.OmzetMerchantDto
	offset := (pagination.Page - 1) * pagination.Limit
	baseQuery := fmt.Sprintf("select MerchantName,\n       Omzet,\n       OmzetDate\nfrom (select m.merchant_name       AS MerchantName,\n             (select IFNULL(SUM(t.bill_total), 0)\n              from Transactions t\n              where t.merchant_id = m.id\n                and cast(DATE(t.created_at) as char) =\n                    selected_date) AS Omzet,\n             selected_date         AS OmzetDate\n      from (select adddate('1970-01-01', t4.i * 10000 + t3.i * 1000 + t2.i * 100 + t1.i * 10 + t0.i) selected_date\n            from (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t0,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t1,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t2,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t3,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t4) v\n               left outer join users u on u.id = @user_id\n               left join Merchants m on u.id = m.user_id\n      where selected_date between '2021-11-01' and '2021-11-30'\n      order by selected_date) as transaction_omzet_merchant")
	var total int64
	db.connection.Raw(baseQuery, sql.Named("user_id", userID)).Count(&total)
	if pagination.Limit > 0 {
		baseQuery = fmt.Sprintf("%s LIMIT %d OFFSET %d", baseQuery, pagination.Limit, offset)
	}
	result := db.connection.Raw(baseQuery, sql.Named("user_id", userID)).Scan(&omzetMerchantDto)
	if result.Error != nil {
		msg := result.Error
		return nil, 0, msg
	}
	return &omzetMerchantDto, total, nil
}

func (db *laporanConnection) FindListLaporanMerchantOutletOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantOutletDto, int64, error) {
	var omzetMerchantOutletDto []dto.OmzetMerchantOutletDto
	offset := (pagination.Page - 1) * pagination.Limit
	baseQuery := fmt.Sprintf("select MerchantName,\n       OutletName,\n       Omzet,\n       OmzetDate\nfrom (select m.merchant_name                                          AS MerchantName,\n             o.outlet_name                                            AS OutletName,\n             (select IFNULL(SUM(t.bill_total), 0)\n              from Transactions t\n              where t.merchant_id = m.id\n                and t.outlet_id = o.id\n                and cast(DATE(t.created_at) as char) = selected_date) AS Omzet,\n             selected_date                                            AS OmzetDate\n      from (select adddate('1970-01-01', t4.i * 10000 + t3.i * 1000 + t2.i * 100 + t1.i * 10 + t0.i) selected_date\n            from (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t0,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t1,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t2,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t3,\n                 (select 0 i\n                  union\n                  select 1\n                  union\n                  select 2\n                  union\n                  select 3\n                  union\n                  select 4\n                  union\n                  select 5\n                  union\n                  select 6\n                  union\n                  select 7\n                  union\n                  select 8\n                  union\n                  select 9) t4) v\n               left outer join users u on u.id = @user_id\n               left join Merchants m on u.id = m.user_id\n               left join Outlets o on m.id = o.merchant_id\n      where selected_date between '2021-11-01' and '2021-11-30'\n      order by selected_date) as transaction_omzet_merchant_outlet ")
	var total int64
	db.connection.Raw(baseQuery, sql.Named("user_id", userID)).Count(&total)
	if pagination.Limit > 0 {
		baseQuery = fmt.Sprintf("%s LIMIT %d OFFSET %d", baseQuery, pagination.Limit, offset)
	}
	result := db.connection.Raw(baseQuery, sql.Named("user_id", userID)).Scan(&omzetMerchantOutletDto)
	if result.Error != nil {
		msg := result.Error
		return nil, 0, msg
	}
	return &omzetMerchantOutletDto, total, nil
}
