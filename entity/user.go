package entity

import "time"

type User struct {
	ID        uint64    `gorm:"primary_key:auto_increment" json:"id"`
	Name      string    `gorm:"type:varchar(45)" json:"name"`
	UserName  string    `gorm:"type:varchar(45)" json:"user_name"`
	Password  string    `gorm:"->;<-;not null" json:"-"`
	CreatedBy uint64    `gorm:"bigint" json:"created_by"`
	CreatedAt time.Time `gorm:"type:timestamp" json:"created_at"`
	UpdatedBy uint64    `gorm:"bigint" json:"updated_by"`
	UpdatedAt time.Time `gorm:"type:timestamp" json:"updated_at"`
	Token     string    `gorm:"-" json:"token,omitempty"`
}
