package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/muhammadyusuf22/testing-backend-majoo/helper"
	"github.com/muhammadyusuf22/testing-backend-majoo/service"
	"github.com/muhammadyusuf22/testing-backend-majoo/utils"
	"log"
	"net/http"
)

type LaporanController interface {
	FindListLaporanMerchantOmzet(context *gin.Context)
	FindListLaporanMerchantOutletOmzet(context *gin.Context)
}

type laporanController struct {
	laporanService service.LaporanService
	jwtService     service.JWTService
}

func NewLaporanController(laporanService service.LaporanService, jwtService service.JWTService) LaporanController {
	return &laporanController{
		laporanService: laporanService,
		jwtService:     jwtService,
	}
}

func (c *laporanController) FindListLaporanMerchantOmzet(context *gin.Context) {
	authHeader := context.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	pagination := utils.GeneratePaginationFromRequest(context)
	if errToken != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	log.Printf("ID USER => %v", id)
	omzetLists, total, err := c.laporanService.FindListLaporanMerchantOmzet(id, &pagination)
	if err != nil {
		res := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObject{})
		context.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	res := helper.BuildResponse(true, "OK", total, omzetLists)
	context.JSON(http.StatusOK, res)
}

func (c *laporanController) FindListLaporanMerchantOutletOmzet(context *gin.Context) {
	authHeader := context.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	pagination := utils.GeneratePaginationFromRequest(context)
	if errToken != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	log.Printf("ID USER => %v", id)
	omzetLists, total, err := c.laporanService.FindListLaporanMerchantOutletOmzet(id, &pagination)
	if err != nil {
		res := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObject{})
		context.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	res := helper.BuildResponse(true, "OK", total, omzetLists)
	context.JSON(http.StatusOK, res)
}
