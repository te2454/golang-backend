package repository

import (
	"log"

	"github.com/muhammadyusuf22/testing-backend-majoo/dto"
	"github.com/muhammadyusuf22/testing-backend-majoo/entity"
	"github.com/muhammadyusuf22/testing-backend-majoo/models"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository interface {
	InsertUser(user entity.User) entity.User
	UpdateUser(user entity.User) entity.User
	VerifyCredential(userName string, password string) interface{}
	IsDuplicateUserName(username string) (tx *gorm.DB)
	FindByUsername(username string) entity.User
	ProfileUser(userID string) entity.User
	GetAllUser() []dto.UserAllDTO
	GetAllUserWithPagination(user *dto.UserAllDTO, pagination *models.Pagination) (*[]dto.UserAllDTO, error)
}

type userConnection struct {
	connection *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userConnection{
		connection: db,
	}
}
func (db *userConnection) InsertUser(user entity.User) entity.User {
	user.Password = hashAndSalt([]byte(user.Password))
	db.connection.Save(&user)
	return user
}

func (db *userConnection) UpdateUser(user entity.User) entity.User {
	if user.Password != "" {
		user.Password = hashAndSalt([]byte(user.Password))
	} else {
		var tempuser entity.User
		db.connection.Find(&tempuser, user.ID)
		user.Password = tempuser.Password
	}
	user.Password = hashAndSalt([]byte(user.Password))
	db.connection.Save(&user)
	return user
}

func (db *userConnection) VerifyCredential(username string, password string) interface{} {
	var user entity.User
	res := db.connection.Where("user_name = ?", username).Take(&user)
	if res.Error == nil {
		return user
	}
	return nil
}

func (db *userConnection) IsDuplicateUserName(username string) (tx *gorm.DB) {
	var user entity.User
	return db.connection.Where("user_name = ?", username).Take(&user)
}

func (db *userConnection) FindByUsername(username string) entity.User {
	var user entity.User
	db.connection.Where("user_name = ?", username).Take(&user)
	return user
}

func (db *userConnection) ProfileUser(userID string) entity.User {
	var user entity.User
	db.connection.Where("id = ?", userID).Take(&user)
	return user
}

func (db *userConnection) GetAllUser() []dto.UserAllDTO {
	var user []dto.UserAllDTO

	db.connection.Raw("select id        AS id,\n       user_name as userName,\n       name      as name\nfrom users").Take(&user)
	return user
}

func (db *userConnection) GetAllUserWithPagination(user *dto.UserAllDTO, pagination *models.Pagination) (*[]dto.UserAllDTO, error) {
	var users []dto.UserAllDTO
	offset := (pagination.Page - 1) * pagination.Limit
	queryBuider := db.connection.Limit(pagination.Limit).Offset(offset).Order(pagination.Sort)
	result := queryBuider.Raw("select id AS id, user_name as userName, name as name\nfrom users").Where(user).Find(&users)
	if result.Error != nil {
		msg := result.Error
		return nil, msg
	}
	return &users, nil
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
		panic("Failed to hash a password")
	}
	return string(hash)
}
