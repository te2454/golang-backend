package entity

import "time"

type Outlet struct {
	ID         uint64    `gorm:"primary_key:auto_increment" json:"id"`
	OutletName string    `gorm:"type:varchar(40)" json:"outlet_name"`
	MerchantID uint64    `gorm:"not null" json:"-"`
	CreatedBy  uint64    `gorm:"bigint" json:"created_by"`
	CreatedAt  time.Time `gorm:"type:timestamp" json:"created_at"`
	UpdatedBy  uint64    `gorm:"bigint" json:"updated_by"`
	UpdatedAt  time.Time `gorm:"type:timestamp" json:"updated_at"`
	Merchant   Merchant  `gorm:"foreignkey:MerchantID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"merchant"`
}
