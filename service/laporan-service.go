package service

import (
	"github.com/muhammadyusuf22/testing-backend-majoo/dto"
	"github.com/muhammadyusuf22/testing-backend-majoo/models"
	"github.com/muhammadyusuf22/testing-backend-majoo/repository"
)

type LaporanService interface {
	FindListLaporanMerchantOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantDto, int64, error)
	FindListLaporanMerchantOutletOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantOutletDto, int64, error)
}

type laporanService struct {
	laporanRepository repository.LaporanRepository
}

//NewLaporanService creates a new instance of LaporanService
func NewLaporanService(laporanRepo repository.LaporanRepository) LaporanService {
	return &laporanService{
		laporanRepository: laporanRepo,
	}
}

func (service *laporanService) FindListLaporanMerchantOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantDto, int64, error) {
	return service.laporanRepository.FindListLaporanMerchantOmzet(userID, pagination)
}

func (service *laporanService) FindListLaporanMerchantOutletOmzet(userID string, pagination *models.Pagination) (*[]dto.OmzetMerchantOutletDto, int64, error) {
	return service.laporanRepository.FindListLaporanMerchantOutletOmzet(userID, pagination)
}
