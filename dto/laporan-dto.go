package dto

type (
	OmzetMerchantDto struct {
		MerchantName string  `db:"merchant_name" json:"merchant_name"`
		Omzet        float64 `db:"omzet" json:"omzet"`
		OmzetDate    string  `db:"omzet_date" json:"omzet_date"`
	}

	OmzetMerchantOutletDto struct {
		MerchantName string  `db:"merchant_name" json:"merchant_name"`
		OutletName   string  `db:"outlet_name" json:"outlet_name"`
		Omzet        float64 `db:"omzet" json:"omzet"`
		OmzetDate    string  `db:"omzet_date" json:"omzet_date"`
	}
)

func (b *OmzetMerchantDto) TableName() string {
	return "transaction_omzet_merchant"
}

func (b *OmzetMerchantOutletDto) TableName() string {
	return "transaction_omzet_merchant_outlet"
}
