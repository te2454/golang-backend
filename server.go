package main

import (
	"github.com/gin-gonic/gin"
	"github.com/muhammadyusuf22/testing-backend-majoo/config"
	"github.com/muhammadyusuf22/testing-backend-majoo/controller"
	"github.com/muhammadyusuf22/testing-backend-majoo/middleware"
	"github.com/muhammadyusuf22/testing-backend-majoo/repository"
	"github.com/muhammadyusuf22/testing-backend-majoo/service"
	"gorm.io/gorm"
)

var (
	db                *gorm.DB                     = config.SetupDatabaseConnection()
	userRepository    repository.UserRepository    = repository.NewUserRepository(db)
	laporanRepository repository.LaporanRepository = repository.NewLaporanRepository(db)
	jwtService        service.JWTService           = service.NewJWTService()
	authService       service.AuthService          = service.NewAuthService(userRepository)
	laporanService    service.LaporanService       = service.NewLaporanService(laporanRepository)
	authController    controller.AuthController    = controller.NewAuthController(authService, jwtService)
	laporanController controller.LaporanController = controller.NewLaporanController(laporanService, jwtService)
)

func main() {
	defer config.CloseDatabaseConnection(db)
	r := gin.Default()

	authRoutes := r.Group("majoo-rest/api/auth")
	{
		authRoutes.POST("/login", authController.Login)
		authRoutes.POST("/register", authController.Register)
	}

	laporanRoutes := r.Group("majoo-rest/api/laporan", middleware.AuthorizationJWT(jwtService))
	{
		laporanRoutes.GET("/merchant-omzet", laporanController.FindListLaporanMerchantOmzet)
		laporanRoutes.GET("/merchant-outlet-omzet", laporanController.FindListLaporanMerchantOutletOmzet)
	}
	r.Run()
}
