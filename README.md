# Test Back End For Majoo

Dibangun Menggunakan Golang dengan framework Gin dan Gorm

## Installation

For Install Package.

```bash
go get
```

###Merubahan Konfigurasi Database Pada file **.env**

## Running

```bash
go run server.go
```

## List package install
```bash
# Gin
go get github.com/gin-gonic/gin

# Godotenv
go get github.com/joho/godotenv

# Gorm
go get gorm.io/gorm

# Mysql Driver
go get gorm.io/driver/mysql

# Golang
go get github.com/golang-jwt/jwt

# Smapping
go get github.com/mashingan/smapping
```

## List API Kebutuhan Test  # (Run Local)
```bash
===================================================================
# Login
Endpoint = http://localhost:8080/majoo-rest/api/auth/login
Request body = {"username": string, "password": string}
Response =
{
    "status": boolean,
    "message": string,
    "error": array string,
    "data": {
        "id": number,
        "name": string,
        "user_name": string,
        "created_by": number,
        "created_at": timestamp,
        "updated_by": number,
        "updated_at": timestamp,
        "token": string
    }
}

===================================================================
# List Merchant Omzet
Endpoint = http://localhost:8080/majoo-rest/api/laporan/merchant-omzet
Request Header = "Authorization"
Query Param = 1. "page" | 2. "limit"
Response = 
{
    "status": boolean,
    "message": string,
    "error": array string,
    "data": [
        {
            "merchant_name": string,
            "omzet": number,
            "omzet_date": string
        }
    ]
}

===================================================================
# List Merchant Outlet Omzet
Endpoint = http://localhost:8080/majoo-rest/api/laporan/merchant-outlet-omzet
Request Header = "Authorization"
Query Param = 1. "page" | 2. "limit"
Response = 
{
    "status": boolean,
    "message": string,
    "error": array string,
    "data": [
        {
            "merchant_name": string,
            "omzet": number,
            "omzet_date": string
        }
    ]
}
```

### Selain API yang ada di Atas, ada beberapa API untuk register dan update user

===================================================================

### Query Untuk Mencari Transaction Merchant Dalam 1 Bulan November
```sql
select MerchantName,
       Omzet,
       OmzetDate
from (select m.merchant_name       AS MerchantName,
             (select IFNULL(SUM(t.bill_total), 0)
              from Transactions t
              where t.merchant_id = m.id
                and cast(DATE(t.created_at) as char) =
                    selected_date) AS Omzet,
             selected_date         AS OmzetDate
      from (select adddate('1970-01-01', t4.i * 10000 + t3.i * 1000 + t2.i * 100 + t1.i * 10 + t0.i) selected_date
            from (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t0,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t1,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t2,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t3,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t4) v
               left outer join users u on u.id = @user_id
               left join Merchants m on u.id = m.user_id
      where selected_date between '2021-11-01' and '2021-11-30'
      order by selected_date) as transaction_omzet_merchant
```

### Query Untuk Mencari Transaction Merchant & Outlet Dalam 1 Bulan November
```sql
select MerchantName,
       OutletName,
       Omzet,
       OmzetDate
from (select m.merchant_name                                          AS MerchantName,
             o.outlet_name                                            AS OutletName,
             (select IFNULL(SUM(t.bill_total), 0)
              from Transactions t
              where t.merchant_id = m.id
                and t.outlet_id = o.id
                and cast(DATE(t.created_at) as char) = selected_date) AS Omzet,
             selected_date                                            AS OmzetDate
      from (select adddate('1970-01-01', t4.i * 10000 + t3.i * 1000 + t2.i * 100 + t1.i * 10 + t0.i) selected_date
            from (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t0,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t1,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t2,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t3,
                 (select 0 i
                  union
                  select 1
                  union
                  select 2
                  union
                  select 3
                  union
                  select 4
                  union
                  select 5
                  union
                  select 6
                  union
                  select 7
                  union
                  select 8
                  union
                  select 9) t4) v
               left outer join users u on u.id = @user_id
               left join Merchants m on u.id = m.user_id
               left join Outlets o on m.id = o.merchant_id
      where selected_date between '2021-11-01' and '2021-11-30'
      order by selected_date) as transaction_omzet_merchant_outlet 
```
