package entity

import "time"

type Merchant struct {
	ID           uint64    `gorm:"primary_key:auto_increment" json:"id"`
	MerchantName string    `gorm:"type:varchar(40)" json:"merchant_name"`
	UserID       uint64    `gorm:"not null" json:"-"`
	CreatedBy    uint64    `gorm:"bigint" json:"created_by"`
	CreatedAt    time.Time `gorm:"type:timestamp" json:"created_at"`
	UpdatedBy    uint64    `gorm:"bigint" json:"updated_by"`
	UpdatedAt    time.Time `gorm:"type:timestamp" json:"updated_at"`
	User         User      `gorm:"foreignkey:UserID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"user"`
}
