package dto

import "gorm.io/gorm"

type UserUpdateDTO struct {
	ID       uint64 `json:"id" form:"id" binding:"required"`
	Name     string `json:"name" form:"name" binding:"required"`
	UserName string `json:"user_name" form:"user_name" binding:"required"`
	Password string `json:"password,omitempty" form:"password,omitempty"`
}

type UserAllDTO struct {
	gorm.Model
	ID       uint64 `db:"id" json:"id"`
	UserName string `db:"user_name" json:"user_name"`
	Name     string `db:"name" json:"name"`
}

func (b *UserAllDTO) TableName() string {
	return "user"
}
