package entity

import "time"

type Transaction struct {
	ID         uint64    `gorm:"primary_key:auto_increment" json:"id"`
	MerchantID uint64    `gorm:"not null" json:"-"`
	OutletID   uint64    `gorm:"not null" json:"-"`
	BillTotal  float64   `gorm:"double" json:"bill_total"`
	CreatedBy  uint64    `gorm:"bigint" json:"created_by"`
	CreatedAt  time.Time `gorm:"type:timestamp" json:"created_at"`
	UpdatedBy  uint64    `gorm:"bigint" json:"updated_by"`
	UpdatedAt  time.Time `gorm:"type:timestamp" json:"updated_at"`
	Merchant   Merchant  `gorm:"foreignkey:MerchantID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"merchant"`
	Outlet     Outlet    `gorm:"foreignkey:OutletID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"outlet"`
}
