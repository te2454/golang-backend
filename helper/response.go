package helper

import (
	"github.com/muhammadyusuf22/testing-backend-majoo/models"
	"strings"
)

type Response struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Error   interface{} `json:"error"`
	Total   int64       `json:"total"`
	Data    interface{} `json:"data"`
}

type EmptyObject struct {
}

type RespDatatables struct {
	pagination models.Pagination
	data       interface{}
}

func BuildDatatablesResponse(status bool, message string, dataPagination RespDatatables) Response {
	res := Response{
		Status:  status,
		Message: message,
		Error:   nil,
		Data:    dataPagination,
	}
	return res
}

func BuildResponse(status bool, message string, total_data int64, data interface{}) Response {
	res := Response{
		Status:  status,
		Message: message,
		Error:   nil,
		Total:   total_data,
		Data:    data,
	}
	return res
}

func BuildErrorResponse(message string, err string, data interface{}) Response {
	splitError := strings.Split(err, "\n")
	res := Response{
		Status:  false,
		Message: message,
		Error:   splitError,
		Total:   0,
		Data:    data,
	}
	return res
}
