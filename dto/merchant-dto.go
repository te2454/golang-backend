package dto

type MerchantUpdateDTO struct {
	ID           uint64 `json:"id" form:"id" binding:"required"`
	MerchantName string `json:"merchant_name" form:"merchant_name" binding:"required"`
	UserID       uint64 `json:"user_id,omitempty" form:"user_id,omitempty"`
}

type MerchantCreateDTO struct {
	ID           uint64 `json:"id" form:"id" binding:"required"`
	MerchantName string `json:"merchant_name" form:"merchant_name" binding:"required"`
	UserID       uint64 `json:"user_id,omitempty" form:"user_id,omitempty"`
}
